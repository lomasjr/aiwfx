from decoder import FaxDecoder

def main():
    decoder = FaxDecoder(calibration_val=8.1595e-05, grayscale=False)
    decoder.decode("sample.wav", "sample.png")
  
if __name__== "__main__":
  main()