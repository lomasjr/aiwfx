# aiwfx

This project is a small library to decode WAV files containing weather fax radio transmissions.  Weather faxes are transmitted around the world on shortwave SSB frequencies as a means to get mariners in the high sea weather information wirelessly.

## Installing

```bash
pip install aiwfx
```


### Getting Started

Simplest way to use it


```python
from aiwfx import FaxDecoder

fd = FaxDecoder()
fd.decode("sample.wav", "sample.png")
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [git](http://git.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **JR Lomas** - [jrlomas](https://github.com/jrlomas)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Special thanks to [P. Lutus](https://arachnoid.com/) for his excellent [java implementation](https://arachnoid.com/JWX/index.html) of a weatherfax software FM demodulator, which was used extensively as the basis for this software.