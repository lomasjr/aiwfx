from enum import Enum
import numpy as np
import wave
import sys
from collections import deque
from PIL import Image

class State(Enum):
    WAITSIG = 0
    WAITSTB = 1
    WAITSTE = 2
    WAITLS1 = 3
    SYNC = 4
    WAITLS2 = 5
    PROC = 6
    END = 7

class FilterType(Enum):
    BANDPASS = 0
    LOWPASS = 1 
    HIGHPASS = 2
    NOTCH = 3 
    PEAK = 4 
    LOWSHELF = 5
    HIGHSHELF = 6

class BiQuadraticFilter:

    def __init__(self, filter_type = None,
                       carrier_frequency = 0.0,
                       sample_rate = 0.0,
                       Q = 0.0,
                       gaindB = 0.0):
        self.a0 = 0.0
        self.a1 = 0.0
        self.a2 = 0.0

        self.b0 = 0.0
        self.b1 = 0.0
        self.b2 = 0.0

        self.x1 = 0.0
        self.x2 = 0.0
        
        self.y = 0.0
        self.y1 = 0.0
        self.y2 = 0.0

        self.filter_type = filter_type
        self.sample_rate = sample_rate
        self.Q = (Q == 0) and 1e-9 or Q
        self.gaindB = gaindB
        # only used for peaking and shelving filters
        gain_abs = pow(10, self.gaindB / 40)
        omega = 2.0 * np.pi * carrier_frequency / self.sample_rate
        sn = np.sin(omega)
        cs = np.cos(omega)
        alpha = sn / (2.0 * self.Q)
        beta = np.sqrt(gain_abs + gain_abs)
        if self.filter_type == FilterType.BANDPASS:
                self.b0 = alpha
                self.b1 = 0.0
                self.b2 = -alpha
                self.a0 = 1.0 + alpha
                self.a1 = -2.0 * cs
                self.a2 = 1.0 - alpha
                
        elif self.filter_type == FilterType.LOWPASS:
                self.b0 = (1.0 - cs) / 2.0
                self.b1 = 1.0 - cs
                self.b2 = (1.0 - cs) / 2.0
                self.a0 = 1.0 + alpha
                self.a1 = -2.0 * cs
                self.a2 = 1.0 - alpha
                
        elif self.filter_type == FilterType.HIGHPASS:
                self.b0 = (1 + cs) / 2
                self.b1 = -(1 + cs)
                self.b2 = (1 + cs) / 2
                self.a0 = 1 + alpha
                self.a1 = -2 * cs
                self.a2 = 1 - alpha

        elif self.filter_type == FilterType.NOTCH:
                self.b0 = 1
                self.b1 = -2 * cs
                self.b2 = 1
                self.a0 = 1 + alpha
                self.a1 = -2 * cs
                self.a2 = 1 - alpha

        elif self.filter_type == FilterType.PEAK:
                self.b0 = 1 + (alpha * gain_abs)
                self.b1 = -2 * cs
                self.b2 = 1 - (alpha * gain_abs)
                self.a0 = 1 + (alpha / gain_abs)
                self.a1 = -2 * cs
                self.a2 = 1 - (alpha / gain_abs)

        elif self.filter_type == FilterType.LOWSHELF:
                self.b0 = gain_abs * ((gain_abs + 1) - (gain_abs - 1) * cs + beta * sn)
                self.b1 = 2 * gain_abs * ((gain_abs - 1) - (gain_abs + 1) * cs)
                self.b2 = gain_abs * ((gain_abs + 1) - (gain_abs - 1) * cs - beta * sn)
                self.a0 = (gain_abs + 1) + (gain_abs - 1) * cs + beta * sn
                self.a1 = -2 * ((gain_abs - 1) + (gain_abs + 1) * cs)
                self.a2 = (gain_abs + 1) + (gain_abs - 1) * cs - beta * sn

        elif self.filter_type == FilterType.HIGHSHELF:
                self.b0 = gain_abs * ((gain_abs + 1) + (gain_abs - 1) * cs + beta * sn)
                self.b1 = -2 * gain_abs * ((gain_abs - 1) + (gain_abs + 1) * cs)
                self.b2 = gain_abs * ((gain_abs + 1) + (gain_abs - 1) * cs - beta * sn)
                self.a0 = (gain_abs + 1) - (gain_abs - 1) * cs + beta * sn
                self.a1 = 2 * ((gain_abs - 1) - (gain_abs + 1) * cs)
                self.a2 = (gain_abs + 1) - (gain_abs - 1) * cs - beta * sn
                
        # prescale flter constants
        self.b0 /= self.a0
        self.b1 /= self.a0
        self.b2 /= self.a0
        self.a1 /= self.a0
        self.a2 /= self.a0

    # perform one filtering step
    def filter(self, x):
        self.y = self.b0 * x + self.b1 * self.x1 + self.b2 * self.x2 - self.a1 * self.y1 - self.a2 * self.y2
        self.x2 = self.x1
        self.x1 = x
        self.y2 = self.y1
        self.y1 = self.y
        return self.y

class GoertzelFilter:
    def __init__(self, frequency, threshold, samples):
        self.samples = samples
        self.threshold = threshold
        self.goertzel_factor = 2 * np.cos(2 * np.pi * frequency)

        # "scaling factor" gives unit result
        # for unit input level

        self.scaling_factor = 4.0 / (samples * samples)
        self.s0 = 0.0
        self.s1 = 0.0 
        self.s2 = 0.0 
        self.samplecount = 0.0
        self.value = 0.0

    def process(self, v):
        self.s0 = v + self.goertzel_factor * self.s1 - self.s2
        self.s2 = self.s1
        self.s1 = self.s0;
        self.samplecount += 1
        if self.samplecount >= self.samples: 
            self.update()

    def update(self):
        self.value = (self.s2 * self.s2 + self.s1 * self.s1 - self.goertzel_factor * self.s1 * self.s2)* self.scaling_factor
        self.s1 = 0.0
        self.s2 = 0.0
        self.samplecount = 0.0
    
    def active(self):
        return (self.value >= self.threshold)


def clock_correct_line(arr, line_offset, delta):
    arr_length = len(arr)
    p = int(delta * float(line_offset))
    p = (arr_length * 32 + p) % arr_length
    if p is not 0:
        x = deque(arr)
        x.rotate(-p)
        return x
    return arr


class FaxDecoder:

    gain_threshold = 256.0
    INVSQR2 = 1.0 / np.sqrt(2)

    def __init__(self, calibration_val = 0,
                       lines_per_second = 2, 
                       ioc = 576, 
                       grayscale = True,
                       goertzel_accept_threshold = 0.30,
                       video_filter = False):
        self.lines_per_second = lines_per_second
        self.ioc = ioc
        self.state = State.WAITSIG
        self.calibration_val = calibration_val
        self.grayscale = grayscale
        self.goertzel_accept_threshold = goertzel_accept_threshold
        self.video_filter = video_filter

    def decode(self, filename, output_filename):
        # Load signal
        spf = wave.open(filename, "r")
        signal = spf.readframes(-1)
        signal = np.fromstring(signal, "Int16")

        print("Number of frames: {}".format(len(signal)))

        #startof: decoder_variable_state
        image_width = int(np.ceil(self.ioc*np.pi))

        sample_count = 0
        sample_rate = spf.getframerate()
        sample_interval = 1.0 / sample_rate
        sample_increment = sample_rate / (image_width * 2.0)

        sync_time = 20 # given in seconds for US broadcast by coastguard
        sync_lines = sync_time * self.lines_per_second
        
        row_length = int(sample_rate / self.lines_per_second)
        gain_level = 0.0
        gain_tc = 1000.0 / sample_rate # time constant gain in milliseconds
        
        pll_reference = 0.0
        pll_loop_gain = 1.0
        pll_integral = 0.0
        pll_center_f = 1900.0 # carrier wave frequency
        pll_deviation_f = 400.0 # deviation from wave frequency
        # produce a unit output level at maximum deviation
        # plus 20% to improve appearance of charts
        pll_output_gain = 1.2 * pll_center_f / pll_deviation_f
        pll_omega = 2.0 * np.pi * pll_center_f

        sig_sum = 0.0
        sig_count = 0

        image_line = 0
        line_index = 0
        line_time_delta = 0
        line_buf = bytearray(image_width)

        sync_array = None
        sync_line = None
        sync_interval = 0.025 # 25 ms in which white level is present followed by 475ms of data
        
        row_index = 0
        row_pos = 0

        pll_output_lowpass_filter_f = 650.0 # in hz
        pll_video_lowpass_filter_f = 400.0 # in hz

        start_f = 300.0 # in Hz
        stop_f = 450.0 # in Hz
        g_size = sample_rate / 4.0
        
        f_g_start = GoertzelFilter(
                start_f * sample_interval,
                self.goertzel_accept_threshold,
                g_size)

        f_g_end = GoertzelFilter(
                stop_f * sample_interval,
                self.goertzel_accept_threshold,
                g_size)

        biquad_pll_output_lowpass = BiQuadraticFilter(filter_type = FilterType.LOWPASS, carrier_frequency = pll_output_lowpass_filter_f, sample_rate = sample_rate, Q = self.INVSQR2)
        biquad_video_lowpass = BiQuadraticFilter(filter_type = FilterType.LOWPASS, carrier_frequency = pll_video_lowpass_filter_f, sample_rate = sample_rate, Q = self.INVSQR2)

        image1d = None
        #endof: decoder_variable_state
        
        for v in signal:
            dv = v
            time_sec = float(sample_count * sample_interval)
            linetime_zero = (((sample_count - int(line_time_delta)) % row_length) == 0)

            # PLL block
            gain_level = gain_level + (abs(dv) - gain_level) * gain_tc
            gain_level = max(.1, gain_level)
            dv = dv / gain_level

            pll_loop_control = dv * pll_reference * pll_loop_gain
            pll_integral += pll_loop_control * sample_interval

            if pll_integral == float("inf"):
                pll_integral = 0.0

            pll_reference = np.sin(pll_omega * (time_sec + pll_integral))
            wsig = biquad_pll_output_lowpass.filter(pll_loop_control) * pll_output_gain

            wsig = min(wsig, 2.0)
            wsig = max(wsig, -2.0)

            f_g_start.process(wsig)
            f_g_end.process(wsig)
            
            # low-pass for video
            sig = self.video_filter and biquad_video_lowpass.filter(wsig) or wsig
            
            #startof: state_machine
            if self.state == State.WAITSIG:
                image_line = 0
                line_time_delta = 0
                if gain_level > self.gain_threshold:
                    self.state = State.WAITSTB
                    print("Entering state of {} at sample: {}".format(self.state, sample_count))

            elif self.state == State.WAITSTB:
                if (gain_level < self.gain_threshold):
                    self.state = State.WAITSIG
                    print("Entering state of {} at sample: {}".format(self.state, sample_count))
                elif f_g_start.active():
                    self.state = State.WAITSTE
                    print("Entering state of {} at sample: {}".format(self.state, sample_count))
                    
            elif self.state == State.WAITSTE:
                if not f_g_start.active():
                    self.state = State.WAITLS1
                    print("Entering state of {} at sample: {}".format(self.state, sample_count))
            
            elif self.state == State.WAITLS1:
                if linetime_zero:
                    sync_array = None
                    self.state = State.SYNC
                    print("Entering state of {} at sample: {}".format(self.state, sample_count))

            elif self.state == State.SYNC:
                if sync_array is None:
                    image_line = 0
                    row_index = 0
                    sync_array = np.zeros(row_length, dtype = float)
                    sync_line = np.zeros(row_length, dtype = float)
            
                # must acculumate to work with
                # noisy signals and clock errors
                
                sync_line[row_index] += wsig
                row_index += 1

                if row_index >= row_length: 
                    row_index = 0
                    # shift result array to correct for clock error
                    sync_line = clock_correct_line(
                            sync_line,
                            image_line,
                            row_length * self.calibration_val)
                            
                    # Accumulate the result
                    for i in range(row_length):
                        sync_array[i] += sync_line[i]
                        sync_line[i] = 0.0
                    
                    if image_line >= sync_lines:
                        # integrate and locate negative excursion
                        iv = sync_array[len(sync_array) - 1]
                        ns = iv > 0 and 1 or -1
                        os = ns
                        # integration time constant
                        # double tc = 200.0 / sample_rate # this is half of the bandwith
                        tc = 200.0 / sample_rate

                        for i in range(row_length):
                            iv += (sync_array[i] - iv) * tc
                            ns = iv > 0 and  1 or -1
                            sync_line[i] = ns - os
                            os = ns
                    
                        v = sync_line[0]
                        line_time_delta = 0
                        for i in range(1,row_length):
                            if sync_line[i] < v:
                                v = sync_line[i]
                                line_time_delta = i

                        # adjust for elapsed time of sync calculation
                        # now align sync bar by subtracting a factor
                        # for the integration time constant
                        line_time_delta += sync_lines * row_length * self.calibration_val - int(sync_interval * 0.12 * sample_rate)
                        self.state = State.WAITLS2
                        print("Entering state of {} at sample: {}".format(self.state, sample_count))

            elif self.state == State.PROC:
                if linetime_zero:
                    row_index = 0
                    row_pos = 0
                    line_index = 0
                    #TODO: Write buffer to file
                    if line_buf is not None:
                        if image1d is None:
                            image1d = line_buf
                        else:
                            line = clock_correct_line(line_buf, len(image1d)/image_width,image_width*self.calibration_val)
                            image1d = np.concatenate((image1d, line), axis=0)

                    line_buf = bytearray(image_width)
                    
                    # criteria for end of processing
                    if f_g_end.active() or image_line > 4000:
                        self.state = State.END
                        print("Entering state of {} at sample: {}".format(self.state, sample_count))
                     
            
                if row_index > int(row_pos):
                    row_pos += sample_increment
                    if sig_count > 0:
                        sig_sum /= float(sig_count)
                    
                    val = (sig_sum + 1.0) * 0.5
                    sig_sum = 0.0
                    sig_count = 0
                    val = min(val, 1.0)
                    val = max(val, 0.0)
                    
                    b = None
                    if self.grayscale:
                        b = int(val * 255.0)
                    else:
                        b = int((val > 0.5) and 255 or 0)
                    
                    line_buf[line_index] = b
                    line_index += 1
                else:
                    sig_sum = float(sig_sum + sig)
                    sig_count += 1
                
                row_index += 1
            
            elif self.state == State.WAITLS2:
                if linetime_zero:
                    #TOOD: Start of new chart
                    row_index = 0
                    row_pos = 0
                    line_index = 0
                    line_buf = bytearray(image_width)
                    sig_sum = 0.0
                    sig_count = 0
                    self.state = State.PROC
                    print("Entering state of {} at sample: {}".format(self.state, sample_count))
            
            elif self.state == State.END:
                image2d = image1d.reshape((int(len(image1d) / image_width), image_width)).astype('uint8')
                im = Image.fromarray(image2d, "L")
                im.save(output_filename)
                self.state = State.WAITSIG
                print("Entering state of {} at sample: {}".format(self.state, sample_count))
            #endof: state_machine

            if linetime_zero:
                image_line += 1
            
            sample_count += 1

